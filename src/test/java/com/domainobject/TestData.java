package com.domainobject;

public class TestData {
    //login data
    public static String username = "Luke";
    public static String password = "Skywalker";

    public static String username1 = "luke";
    public static String password1 = "Skywalker";

    public static String errormessage = "Invalid username or password!";

    public static String hello = "Hello Luke";

    //user creation data
    public static String firstname = "Username11";
    public static String lastname = "User last name";
    public static String startdate = "2010-01-01";
    public static String email = "test_email@gmail.com";
}
