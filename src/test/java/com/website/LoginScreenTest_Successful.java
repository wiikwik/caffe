package com.website;

import com.domainobject.TestAppEnv;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import static com.domainobject.TestData.hello;
import static com.domainobject.TestData.username;
import static com.domainobject.TestData.password;
import static org.junit.Assert.assertEquals;


public class LoginScreenTest_Successful {
    private String[] TestData;


    @Test

    //Open Web site link
    public void checkTitleCorrectOnApp() throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", "D:\\git\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.get(TestAppEnv.getUrl());
        ;
        Thread.sleep(1000L);
        assertEquals("Title should match",
                "CafeTownsend-AngularJS-Rails", driver.getTitle());

        Login(driver, username, password);

    }

    //Login into application
    public void Login(WebDriver driver, String username, String password) throws InterruptedException {
        System.out.println("Begin");

        WebElement username_field = driver.findElement(By.cssSelector("#login-form > fieldset > label:nth-child(3) > input"));
        username_field.sendKeys(username);

        WebElement password_field = driver.findElement(By.cssSelector("#login-form > fieldset > label:nth-child(4) > input"));
        password_field.sendKeys(password);

        WebElement login_button = driver.findElement(By.cssSelector("#login-form > fieldset > button"));
        login_button.click();

        WebElement hello_field = driver.findElement(By.cssSelector("#greetings"));

        Thread.sleep(1000L);
        if (hello_field.getText().equals(hello)) {
            System.out.println("PASSED ");
            assert true;
        } else {
            System.out.println("FAILED " + "actual=" + hello_field.getText() + " " + "expected= " +hello );
            assert false;
        }

    }


    }












