package com.website;

import com.domainobject.TestAppEnv;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static com.domainobject.TestData.hello;
import static com.domainobject.TestData.password;
import static com.domainobject.TestData.username;
import static org.junit.Assert.assertEquals;

public class LogoutTest {

    private String[] TestData;

    @Test

    //Open Web site link
    public void checkTitleCorrectOnApp() throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", "D:\\git\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.get(TestAppEnv.getUrl());

        Thread.sleep(1000L);
        assertEquals("Title should match",
                "CafeTownsend-AngularJS-Rails", driver.getTitle());

        Login(driver, username, password);

    }

    ////Login into application
    public void Login(WebDriver driver, String username, String password) throws InterruptedException {
        System.out.println("Begin");

        WebElement username_field = driver.findElement(By.cssSelector("#login-form > fieldset > label:nth-child(3) > input"));
        username_field.sendKeys(username);

        WebElement password_field = driver.findElement(By.cssSelector("#login-form > fieldset > label:nth-child(4) > input"));
        password_field.sendKeys(password);

        WebElement login_button = driver.findElement(By.cssSelector("#login-form > fieldset > button"));
        login_button.click();
        Thread.sleep(1000L);

//Logout
        System.out.println("Logout");
        WebElement loguot_button = driver.findElement(By.cssSelector("body > div > header > div > p.main-button"));
        loguot_button.click();
        Thread.sleep(2000L);

        WebElement login_button1 = driver.findElement(By.cssSelector("#login-form > fieldset > button"));
        if (login_button1.getText().equals("Login")) {
            System.out.println("PASSED ");
            assert true;
        } else {
            System.out.println("FAILED " + "actual=" + login_button1.getText() + " " + "expected= Login");
            assert false;
        }

    }

}

