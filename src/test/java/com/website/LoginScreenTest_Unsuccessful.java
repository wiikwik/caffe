package com.website;

import com.domainobject.TestAppEnv;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static com.domainobject.TestData.*;
import static org.junit.Assert.assertEquals;

public class LoginScreenTest_Unsuccessful {

         private String[] TestData;


        @Test

        //Open Web site link
        public void checkTitleCorrectOnApp() throws InterruptedException {

            System.setProperty("webdriver.chrome.driver", "D:\\git\\chromedriver.exe");
            WebDriver driver = new ChromeDriver();

            driver.get(TestAppEnv.getUrl());
            ;
            Thread.sleep(1000L);
            assertEquals("Title should match",
                    "CafeTownsend-AngularJS-Rails", driver.getTitle());

            Login(driver, username, password);

        }

        //Login into application
        public void Login(WebDriver driver, String username, String password) throws InterruptedException {
            System.out.println("Begin");

            WebElement username_field = driver.findElement(By.cssSelector("#login-form > fieldset > label:nth-child(3) > input"));
            username_field.sendKeys(username1);

            WebElement password_field = driver.findElement(By.cssSelector("#login-form > fieldset > label:nth-child(4) > input"));
            password_field.sendKeys(password1);

            WebElement login_button = driver.findElement(By.cssSelector("#login-form > fieldset > button"));
            login_button.click();

            WebElement error_message = driver.findElement(By.cssSelector("#login-form > fieldset > p.error-message.ng-binding"));


            Thread.sleep(1000L);


            if (error_message.getText().equals(errormessage)) {
                System.out.println("PASSED");
                assert true;
            } else {
                System.out.println("FAILED " + "actual=" + error_message.getText() + " " + " expected= " + errormessage);
                assert false;
            }

        }



    }

