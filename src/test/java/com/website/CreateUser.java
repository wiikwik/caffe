package com.website;

import com.domainobject.TestAppEnv;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static com.domainobject.TestData.*;
import static org.junit.Assert.assertEquals;

public class CreateUser {

    private String[] TestData;


    @Test

    //Open Web site link
    public void checkTitleCorrectOnApp() throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", "D:\\git\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.get(TestAppEnv.getUrl());
        ;
        Thread.sleep(1000L);
        assertEquals("Title should match",
                "CafeTownsend-AngularJS-Rails", driver.getTitle());

        Login(driver, username, password);

    }

    //Login into application
    public void Login(WebDriver driver, String username, String password) throws InterruptedException {
        System.out.println("Begin");

        WebElement username_field = driver.findElement(By.cssSelector("#login-form > fieldset > label:nth-child(3) > input"));
        username_field.sendKeys(username);

        WebElement password_field = driver.findElement(By.cssSelector("#login-form > fieldset > label:nth-child(4) > input"));
        password_field.sendKeys(password);

        WebElement login_button = driver.findElement(By.cssSelector("#login-form > fieldset > button"));
        login_button.click();

        Thread.sleep(1000L);

//Creating user
        WebElement create_button = driver.findElement(By.cssSelector("#bAdd"));
        create_button.click();
        Thread.sleep(1000L);

        WebElement firstname_field = driver.findElement(By.cssSelector("body > div > div > div > form > fieldset > label:nth-child(3) > input"));
        firstname_field.sendKeys(firstname);

        WebElement lastname_field = driver.findElement(By.cssSelector("body > div > div > div > form > fieldset > label:nth-child(4) > input"));
        lastname_field.sendKeys(lastname);

        WebElement startdate_field = driver.findElement(By.cssSelector("body > div > div > div > form > fieldset > label:nth-child(5) > input"));
        startdate_field.sendKeys(startdate);

        WebElement email_field = driver.findElement(By.cssSelector("body > div > div > div > form > fieldset > label:nth-child(6) > input"));
        email_field.sendKeys(email);

        WebElement add_button = driver.findElement(By.cssSelector("body > div > div > div > form > fieldset > div > button:nth-child(2)"));
        add_button.click();

//checking that user added  -doesnt work.
        Thread.sleep(1000L);
        driver.switchTo().frame("iframe");
        WebElement user_added = driver.findElement(By.partialLinkText(firstname+ " " + lastname));

//comparing with test data
        Thread.sleep(1000L);
        //assertEquals(firstname,user_added.getText() );

if (user_added.getText().equals(firstname+lastname)) {
        System.out.println("PASSED ");
        assert true;
    } else {
        System.out.println("FAILED " + "actual=" + user_added.getText() + " " + "expected= " +firstname + " " +lastname);
        assert false;
    }

    }
}













